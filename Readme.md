## Testing Instructions ( please open with a markdown viewer if possible )

## Installation

```bash
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

## To check the first exercise (`utils.py`)

 ```bash
 python utils/test_utils.py
 ```

## To check CRUD operations

 ```bash
 pytest crud/test_api.py
 ```

## Starting the FastAPI server

 ```bash
 python crud/main.py
 ```

## Some example requests using CURL

### Create Course

 ```bash
 curl localhost:8000/courses/ -X POST -H "Content-Type: application/json" -d '{"name": "New Course Name", "id": 1040, "price": 1499.99, "details": {"duration": "10 weeks", "level": "Advanced", "language": "English"}}'
 ```

### Retrieve All Courses

 ```bash
 curl localhost:8000/courses/
 ```

### Retrieve Specific Course (by ID)

 ```bash
 curl localhost:8000/courses/1040
 ```

### Update Course (by ID)

 ```bash
 curl localhost:8000/courses/1040 -X PUT -H "Content-Type: application/json" -d '{"name": "Updated Course Name", "id": 1040, "price": 1699.99, "details": {"duration": "12 weeks", "level": "Advanced", "language": "English"}}'
 ```

### Delete Course (by ID)

 ```bash
 curl localhost:8000/courses/1040 -X DELETE
 ```

### Create Student

 ```bash
 curl localhost:8000/students/ -X POST -H "Content-Type: application/json" -d '{"name": "New Student Name", "student_id": 300, "average_grade": 16.5, "courses": {"courseId": [101, 103], "progression": {"101": 50, "103": 75}}}'
 ```

### Retrieve All Students

 ```bash
 curl localhost:8000/students/
 ```

### Retrieve Specific Student (by ID)

 ```bash
 curl localhost:8000/students/300
 ```

### Update Student (by ID)

 ```bash
 curl localhost:8000/students/300 -X PUT -H "Content-Type: application/json" -d '{"name": "Updated Student Name", "student_id": 300, "average_grade": 17.2, "courses": {"courseId": [101, 103], "progression": {"101": 75, "103": 100}}}'
 ```

### Delete Student (by ID)

 ```bash
 curl localhost:8000/students/300 -X DELETE
 ```