from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from typing import List, Dict, Any
import json

app = FastAPI()

class Course(BaseModel):
    name: str
    id: int
    price: float
    details: Dict[str, str]

class Student(BaseModel):
    name: str
    student_id: int
    average_grade: float
    courses: Dict[str, Any]

course_path = "crud/data/courses.json"
student_path = "crud/data/students.json"

# Load initial data from JSON files
with open(course_path, "r") as courses_file:
    courses_db = json.load(courses_file)

with open(student_path, "r") as students_file:
    students_db = json.load(students_file)

def save_courses():
    with open(course_path, "w") as courses_file:
        json.dump(courses_db, courses_file, indent=4)

def save_students():
    with open(student_path, "w") as students_file:
        json.dump(students_db, students_file, indent=4)

@app.post("/courses/")
def create_course(course: Course):
    for existing_course in courses_db:
        if existing_course["id"] == course.id:
            raise HTTPException(status_code=400, detail="Course with this ID already exists")
    courses_db.append(dict(course))
    save_courses()
    return course

@app.get("/courses/", response_model=List[Course])
def get_courses():
    return courses_db

@app.get("/courses/{course_id}", response_model=Course)
def get_course(course_id: int):
    for course in courses_db:
        if course["id"] == course_id:
            return course
    raise HTTPException(status_code=404, detail="Course not found")

@app.put("/courses/{course_id}")
def update_course(course_id: int, course: Course):
    for idx, c in enumerate(courses_db):
        if c["id"] == course_id:
            courses_db[idx] = dict(course)
            save_courses()
            return {"message": "Course updated successfully"}
    raise HTTPException(status_code=404, detail="Course not found")

@app.delete("/courses/{course_id}")
def delete_course(course_id: int):
    for idx, course in enumerate(courses_db):
        if course["id"] == course_id:
            del courses_db[idx]
            save_courses()
            return {"message": "Course deleted successfully"}
    raise HTTPException(status_code=404, detail="Course not found")

@app.post("/students/")
def create_student(student: Student):
    for existing_student in students_db:
        if existing_student["student_id"] == student.student_id:
            raise HTTPException(status_code=400, detail="Student with this ID already exists")
    students_db.append(dict(student))
    save_students()
    return student

@app.get("/students/", response_model=List[Student])
def get_students():
    return students_db

@app.get("/students/{student_id}", response_model=Student)
def get_student(student_id: int):
    for student in students_db:
        if student["student_id"] == student_id:
            return student
    raise HTTPException(status_code=404, detail="Student not found")

@app.put("/students/{student_id}")
def update_student(student_id: int, student: Student):
    for idx, s in enumerate(students_db):
        if s["student_id"] == student_id:
            students_db[idx] = dict(student)
            save_students()
            return {"message": "Student updated successfully"}
    raise HTTPException(status_code=404, detail="Student not found")

@app.delete("/students/{student_id}")
def delete_student(student_id: int):
    for idx, student in enumerate(students_db):
        if student["student_id"] == student_id:
            del students_db[idx]
            save_students()
            return {"message": "Student deleted successfully"}
    raise HTTPException(status_code=404, detail="Student not found")

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)