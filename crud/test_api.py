import pytest
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

@pytest.mark.parametrize(
    "test_input, expected_output",
    [
        ({"name": "New Course", "id": 3, "price": 149.99, "details": {"duration": "8 weeks", "level": "Advanced", "language": "French"}}, {"status_code": 200}),
        ({"name": "Updated Course", "id": 1, "price": 69.99, "details": {"duration": "6 weeks", "level": "Intermediate", "language": "English"}}, {"status_code": 200}),
    ],
)
def test_course_crud(test_input, expected_output):
    # Create
    response = client.post("/courses/", json=test_input)
    assert response.status_code == expected_output["status_code"]
    if response.status_code == 200:
        course_id = response.json()["id"]

        # Get
        response = client.get(f"/courses/{course_id}")
        assert response.status_code == 200
        assert response.json() == test_input

        # Update
        updated_input = {**test_input, "name": "Updated Name"}
        response = client.put(f"/courses/{course_id}", json=updated_input)
        assert response.status_code == 200
        assert response.json() == {"message": "Course updated successfully"}

        # Get
        response = client.get(f"/courses/{course_id}")
        assert response.status_code == 200
        assert response.json() == {**test_input, "name": "Updated Name"}

        # Delete
        response = client.delete(f"/courses/{course_id}")
        assert response.status_code == 200
        assert response.json() == {"message": "Course deleted successfully"}

@pytest.mark.parametrize(
    "test_input, expected_output",
    [
        ({"name": "Charlie", "student_id": 103, "average_grade": 88.0, "courses": {"courseId": [1, 2], "progression": {"1": 0.5, "2": 0.25}}}, {"status_code": 200}),
        ({"name": "Alice Smith", "student_id": 101, "average_grade": 92.0, "courses": {"courseId": [1, 2], "progression": {"1": 0.9, "2": 0.75}}}, {"status_code": 200}),
    ],
)
def test_student_crud(test_input, expected_output):
    # Create
    response = client.post("/students/", json=test_input)
    assert response.status_code == expected_output["status_code"]
    if response.status_code == 200:
        student_id = response.json()["student_id"]

        # Get
        response = client.get(f"/students/{student_id}")
        assert response.status_code == 200
        assert response.json() == test_input

        # Update
        updated_input = {**test_input, "name": "Updated Name"}
        response = client.put(f"/students/{student_id}", json=updated_input)
        assert response.status_code == 200
        assert response.json() == {"message": "Student updated successfully"}

        # Get
        response = client.get(f"/students/{student_id}")
        assert response.status_code == 200
        assert response.json() == {**test_input, "name": "Updated Name"}

        # Delete
        response = client.delete(f"/students/{student_id}")
        assert response.status_code == 200
        assert response.json() == {"message": "Student deleted successfully"}